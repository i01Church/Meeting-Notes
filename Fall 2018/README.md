Fall 2018 Meeting Schedule:

Sept. 6th   	Big Picture

Sept. 13th  	Linux Command Line

Sept. 20th  	File Permissions / SSH

Sept. 27th  	Firewalls

Oct. 4th    	FALL BREAK - NO MEETING

Oct. 11th   	Scanning and Enumeration / Fantastic Backdoors (and where to find them)

Oct. 18th   	Apache Web Server

Oct. 25th   	Vulnerable Web Applications / Services

Nov. 1st    	Vulnerable Web Applications / Services

Nov. 8th    	Exploitation / Persistence

Nov. 15th   	Password Cracking

Thanksgiving Break!! YAY NO SCHOOL!

Nov. 29th   	Bash Scripting

Meeting Notes and other pertinent information will be posted.